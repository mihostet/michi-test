/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;

public class XWindowsTest {

    @Test
    public void allWindowsContainsElements() {
        Set<Long> windowIds = XWindows.allWindowIds();
        System.out.println("All window Ids: " + windowIds);
        assertFalse(windowIds.isEmpty());
    }

    @Test
    public void allWindowTitles() {
        Set<Long> windowIds = XWindows.allWindowIds();
        CommandlineXWindowUtil windowUtil = new CommandlineXWindowUtil();
        Map<Long, String> idTitles = windowUtil.windowTitlesOfIds(windowIds);
        System.out.println("Window Ids -> titles: " + idTitles);
        assertFalse(idTitles.isEmpty());
    }

    @Test
    public void xclockFound() {
        XWindowUtil windowUtil = new CommandlineXWindowUtil();
        Optional<Long> windowId = windowUtil.findWindowTitleContains("xclock");
        assertTrue(windowId.isPresent());
    }

}
