/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import static org.junit.Assert.*;

import org.junit.Test;

public class CommandlineXWindowUtilTest {

    @Test
    /* Works only if run from eclipse ;-) */
    public void eclipseIsActiveWindow() {
        XWindowUtil tool = new CommandlineXWindowUtil();

        long id = tool.idOfActiveWindow();

        String activeWindowTitle = tool.windowTitleOfId(id);
        System.out.println("window title is: " + activeWindowTitle);
        assertTrue(activeWindowTitle.contains("Eclipse Platform"));
    }

}
