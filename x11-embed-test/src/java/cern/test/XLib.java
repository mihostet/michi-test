/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

public interface XLib {
    void XReparentWindow(long display, long window, long parent, int x, int y);

    void XUnmapWindow(long display, long window);

    void XMapWindow(long display, long window);

    void XSync(long display, int discard);

    void XResizeWindow(long display, long window, int width, int height);

    void XSetInputFocus(long display, long window);

    long XGetInputFocus(long display);

    String GetProperty(long display, long window, long atom);

    long InternAtom(long display, String string, int only_if_exists);
}
