/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

public interface XEmbedCanvasPeerBackdoor {
    boolean isXEmbedActive();
}
