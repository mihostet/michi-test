/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import sun.awt.X11.XAtom;
import sun.awt.X11.XToolkit;

public class XlibXWindowUtil implements XWindowUtil {
    private static final XLib XLIB = ReflectionMagic.backdoorToStaticMethodsOf("sun.awt.X11.XlibWrapper", XLib.class);
    private static final XLibUtil XLIB_UTIL = ReflectionMagic.backdoorToStaticMethodsOf("sun.awt.X11.XlibUtil",
            XLibUtil.class);
    private final long DISPLAY = XToolkit.getDisplay();

    @Override
    public String windowTitleOfId(long id) {
        String windowTitle = XLIB.GetProperty(DISPLAY, id, XAtom.XA_WM_NAME);
        if (windowTitle != null) {
            return windowTitle;
        }
        return "";
    }

    @Override
    public long idOfActiveWindow() {
        long wid = XLIB.XGetInputFocus(DISPLAY);
        while (wid > 0 && windowTitleOfId(wid) == null) {
            wid = XLIB_UTIL.getParentWindow(wid);
        }
        return wid;
    }

    public static void main(String[] args) {
        XWindowUtil tool = new XlibXWindowUtil();

        long id = tool.idOfActiveWindow();

        System.out.println("window title is: " + tool.windowTitleOfId(id));
    }
}
