/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Optional;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import sun.awt.X11.XEmbedCanvasPeer;
import sun.awt.X11.XToolkit;
import sun.util.logging.PlatformLogger;
import sun.util.logging.PlatformLogger.Level;

public class X11EmbedTest {

    private static final String XCLOCK_TITLE = "xclock";

    public static void main(String[] args) throws Exception {
        System.setProperty("sun.awt.xembedserver", "true");
        PlatformLogger.getLogger("").setLevel(Level.ALL);
        PlatformLogger.getLogger("sun.awt.X11.xembed.XEmbedCanvasPeer").setLevel(Level.ALL);

        JFrame frame = new JFrame();
        frame.setVisible(true);
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        Canvas canvas = new Canvas();
        canvas.setFocusable(true);
        frame.add(canvas, BorderLayout.CENTER);

        JTextField additionalInput = new JTextField("Edit Me");
        frame.add(additionalInput, BorderLayout.NORTH);

        XLib xlib = ReflectionMagic.backdoorToStaticMethodsOf("sun.awt.X11.XlibWrapper", XLib.class);

        XWindowUtil xWindowUtil = new XlibXWindowUtil();

        Optional<Long> xClockWindowId = xWindowUtil.findWindowTitleContains(XCLOCK_TITLE);
        if (!xClockWindowId.isPresent()) {
            throw new RuntimeException("No window of title '" + XCLOCK_TITLE + "' could be found.");
        }

        long wid = xClockWindowId.get();
        long display = XToolkit.getDisplay();
        xlib.XUnmapWindow(display, wid);
        xlib.XSync(display, 0);
        Thread.sleep(300); // Poor man's "wait for WM_STATE property to be WithdrawnState" (FIXME)

        XEmbedCanvasPeer peer = (XEmbedCanvasPeer) canvas.getPeer();
        xlib.XReparentWindow(display, wid, peer.getWindow(), 0, 0);
        XEmbedCanvasPeerBackdoor peerBackdoor = ReflectionMagic.backdoorTo(peer, XEmbedCanvasPeerBackdoor.class);
        while (!peerBackdoor.isXEmbedActive()) {
            Thread.sleep(100);
        }
        xlib.XMapWindow(display, wid);
        xlib.XSync(display, 0);
        xlib.XResizeWindow(display, wid, canvas.getWidth(), canvas.getHeight());
        if (canvas.hasFocus()) {
            xlib.XSetInputFocus(display, wid);
        }

        canvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                xlib.XResizeWindow(display, wid, canvas.getWidth(), canvas.getHeight());
            }
        });

        canvas.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                xlib.XSetInputFocus(display, wid);
            }
        });
        //
        // canvas.addMouseListener(new MouseAdapter() {
        // @Override
        // public void mouseEntered(MouseEvent e) {
        // System.out.println("requesting focus");
        // canvas.requestFocus();
        // }
        // });

        frame.setSize(499, 499);
    }
}
