/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import java.util.Set;

public interface XLibUtil {

    Set<Long> getChildWindows(long windowId);

    long getParentWindow(long window);
}
