/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

public interface XWindowUtil {

    String windowTitleOfId(long id);

    default Optional<Long> findWindowTitleContains(String title) {
        Set<Long> matchingIds = XWindows.allWindowIds().stream().filter(id -> windowTitleOfId(id).contains(title))
                .collect(toSet());
        if (matchingIds.size() > 1) {
            throw new IllegalStateException("More than one window ids match the given title snippet '" + title
                    + "'. Ids: " + matchingIds + ".");
        }
        return matchingIds.stream().findAny();
    }

    default Map<Long, String> allWindowIdsWithTitles() {
        return windowTitlesOfIds(XWindows.allWindowIds());
    }

    default Map<Long, String> windowTitlesOfIds(Collection<Long> ids) {
        return ids.stream().collect(toMap(identity(), this::windowTitleOfId));
    }

    long idOfActiveWindow();

}