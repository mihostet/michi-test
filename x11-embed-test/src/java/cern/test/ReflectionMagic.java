/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ReflectionMagic {

    private ReflectionMagic() {
        /* static method */
    }

    @SuppressWarnings("unchecked")
    private static <T> T createBackdoor(Class<T> interfaceToImplement, Class<?> backingClass, Object instance) {
        Map<Method, Method> interfaceToBackdoorMap = new HashMap<>();
        try {
            for (Method method : interfaceToImplement.getDeclaredMethods()) {
                Method backingMethod = backingClass.getDeclaredMethod(method.getName(), method.getParameterTypes());
                backingMethod.setAccessible(true);
                interfaceToBackdoorMap.put(method, backingMethod);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return (T) Proxy.newProxyInstance(ReflectionMagic.class.getClassLoader(),
                new Class<?>[] { interfaceToImplement }, (Object proxy, Method method, Object[] args) -> {
                    return interfaceToBackdoorMap.get(method).invoke(instance, args);
                });
    }

    public static <T> T backdoorToStaticMethodsOf(Class<?> clazz, Class<T> backdoorInterface) {
        return createBackdoor(backdoorInterface, clazz, null);
    }

    public static <T> T backdoorToStaticMethodsOf(String className, Class<T> backdoorInterface) {
        try {
            return createBackdoor(backdoorInterface, Class.forName(className), null);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T backdoorTo(Object instance, Class<T> backdoorInterface) {
        return createBackdoor(backdoorInterface, instance.getClass(), instance);
    }
}
