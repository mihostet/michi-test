/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import java.util.HashSet;
import java.util.Set;

import sun.awt.X11.XToolkit;

public class XWindows {

    private static final XLibUtil XLIB_UTIL = ReflectionMagic.backdoorToStaticMethodsOf("sun.awt.X11.XlibUtil",
            XLibUtil.class);

    private XWindows() {
        /* Only static methods */
    }

    public static final Set<Long> allWindowIds() {
        return findChildrenIds(XToolkit.getDefaultRootWindow());
    }

    private static final Set<Long> findChildrenIds(long parentId) {
        HashSet<Long> allIds = new HashSet<>();
        allIds.add(parentId);
        Set<Long> children = XLIB_UTIL.getChildWindows(parentId);
        for (Long childId : children) {
            allIds.addAll(findChildrenIds(childId));
        }
        return allIds;

    }

}
