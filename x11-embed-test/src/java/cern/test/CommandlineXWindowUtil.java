/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This implementation of the window util uses command line tools to get the window title of a specific window id. This
 * for sure is not perfect, but should work for the moment.
 * <p>
 * Inspired by: http://stackoverflow.com/questions/1354254/get-current-active-windows-title-in-java
 * <p>
 * NOTE: Works only on linux of course ;-)
 * 
 * @author kfuchsbe
 */
public class CommandlineXWindowUtil implements XWindowUtil {

    public static final String ACTIVE_WIN_ID_CMD = "xprop -root | grep " + "\"_NET_ACTIVE_WINDOW(WINDOW)\""
            + "|cut -d ' ' -f 5";
    public static final String WIN_INFO_CMD_PREFIX = "xwininfo -id ";
    public static final String WIN_TITLE_CMD_MID = " |awk \'BEGIN {FS=\"\\\"\"}/xwininfo: Window id/{print $2}\' | sed \'s/-[^-]*$//g\'";

    private String execShellCmd(String cmd) {
        try {

            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(new String[] { "/bin/bash", "-c", cmd });
            int exitValue = process.waitFor();
            if (exitValue != 0) {
                throw new RuntimeException(
                        "command '" + cmd + "' finished with exit value " + exitValue + ". Cannot continue.");
            }
            BufferedReader buf = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            String output = "";
            while ((line = buf.readLine()) != null) {
                output = line;
            }
            return output;
        } catch (Exception e) {
            throw new RuntimeException("Error while executing command '" + cmd + "'. Cannot continue.", e);
        }
    }

    private String windowTitleCommand(String winId) {
        if (null != winId && !"".equalsIgnoreCase(winId)) {
            return WIN_INFO_CMD_PREFIX + winId + WIN_TITLE_CMD_MID;
        }
        return null;
    }

    @Override
    public long idOfActiveWindow() {
        return Long.decode(execShellCmd(ACTIVE_WIN_ID_CMD));
    }

    @Override
    public String windowTitleOfId(long id) {
        String cmd = windowTitleCommand(String.valueOf(id));
        return execShellCmd(cmd);
    }

    public static void main(String[] args) {
        XWindowUtil tool = new CommandlineXWindowUtil();

        long id = tool.idOfActiveWindow();

        System.out.println("window title is: " + tool.windowTitleOfId(id));

    }

}
