    public static ImmutableDiscreteFunction reduceFunctionPoints(ImmutableDiscreteFunction function,
                                                                 int functionSizeThreshold,
                                                                 double initialFilterThreshold,
                                                                 double filterToleranceIncrement,
                                                                 int maxIterations) {
        Assert.argNotNull(function, "function");
        Assert.isTrue(functionSizeThreshold > 1, "The max number of points must be grater than 1");
        Assert.isTrue(filterToleranceIncrement > 0, "The filter tolerance increment must be grater than 0");

        if (function.size() <= functionSizeThreshold) {
            return function;
        }

        DiscreteFunction filtered = null;
        int iteration = 0;
        double tolerance = initialFilterThreshold;
        do {
            filtered = (DiscreteFunction) function.makeMutable();
            DiscreteFunctions.FilterResult result = filter(filtered, tolerance);
            if (result == ONLY_FLAT_TOPS) {
                throw new IllegalArgumentException(
                        String.format("The function can't be reduced to %d points, it contains too many flat vectors.",
                                functionSizeThreshold));
            }
            if (iteration > maxIterations) {
                throw new IllegalStateException(
                        String.format("The function can't be reduced to %d points - max. iterations (%d) reached, %d points left.",
                                functionSizeThreshold, maxIterations, filtered.size()
                        ));
            }
            tolerance *= 1 + filterToleranceIncrement;
            iteration++;
        } while (filtered.size() > functionSizeThreshold);
        System.out.println(tolerance);
        return filtered;
    }
